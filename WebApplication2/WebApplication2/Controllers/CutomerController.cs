﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2;
using WebApplication2.Core.Abstractions;
using WebApplication2.Core.Entities;
using WebApplication2.Core.Models;

namespace WebApplication6.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly ICustomerBL _customerBL;

        public StudentsController(ICustomerBL studentBL)
        {
            _customerBL = studentBL;
        }

        [HttpPost]
        public IActionResult AddStudent([FromBody] Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var addedStudent = _customerBL.AddStudent(customer);

            return Created("", addedStudent);
        }

        [HttpGet]
        public IActionResult GetStudents([FromQuery] CutomerFilterModel filterModel)
        {
            var students = _customerBL.GetStudents(filterModel);

            return Ok(students);
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveStudent([FromRoute] int id)
        {
            _customerBL.RemoveStudent(id);

            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult EditStudent([FromRoute] int id, [FromBody] Customer customer)
        {
            _customerBL.EditStudent(id, customer);

            return Ok();
        }
    }
}