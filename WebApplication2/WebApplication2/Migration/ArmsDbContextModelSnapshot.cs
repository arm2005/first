﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebApplication2;

namespace WebApplication2.Migration
{
    [DbContext(typeof(ArmsDbContext))]
    partial class ArmsDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApplication6.Entities.Student", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("int")
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<int>("Age")
                    .HasColumnType("INTEGER");

                b.Property<string>("City")
                    .HasColumnType("NVARCHAR(50)");

                b.Property<string>("Country")
                    .HasColumnType("NVARCHAR(50)");

                b.Property<string>("FirstName")
                    .HasColumnType("NVARCHAR(50)");

                b.Property<string>("LastName")
                    .HasColumnType("NVARCHAR(50)");

                b.HasKey("Id");

                b.ToTable("Students");
            });
#pragma warning restore 612, 618
        }
    }
}
