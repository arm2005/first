﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Entities
{
    [MyCustom]
    public class Cutomer
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [Range(1, 50)]
        public int Age { get; set; }
        [Required]
        public string Country { get; set; }
        public string City { get; set; }
    }
}
