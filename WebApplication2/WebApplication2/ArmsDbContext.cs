﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Entities;

namespace WebApplication2
{
    public class ArmsDbContext
    {
        public ArmsDbContext(DbContextOptions options)
              : base(options)
        {

        }
        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(x =>
            {
                x.ToTable("Customers");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.FirstName).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.LastName).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.Country).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.City).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.Age).HasColumnType("INTEGER");
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
