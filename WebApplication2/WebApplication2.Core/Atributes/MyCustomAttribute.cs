﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.entities;

namespace WebApplication6
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MyCustomAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is Customer s)
            {
                return s.City != "Moscow";
            }

            return true;
        }
    }
}