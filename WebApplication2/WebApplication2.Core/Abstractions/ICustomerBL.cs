﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication2.Core.Entities;
using WebApplication2.Core.Models;

namespace WebApplication6.Core.Abstractions
{
    public interface IStudentBL
    {
        IEnumerable<Customer> GetStudents(CustomerFilterModel filterModel);
        bool RemoveStudent(int id);
        bool EditStudent(int id, Customer Customer);
        Customer AddStudent(Customer customer);
    }
}