﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            String writeData = "My First object";
            FileStream writeStream = new FileStream("D:\\A.data", FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(writeStream, writeData);
            writeStream.Close();
        }
    }
}
