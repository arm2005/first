﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApplication6.Core.Abstractions;
using WebApplication6.Core.Abstractions.Operations;
using WebApplication6.Core.Entities;
using WebApplication6.Core.Exceptions;
using WebApplication6.Core.Models;

namespace WebApplication6.BLL.Operations
{
    public class UserBL : IUserBL
    {
        private readonly IRepositoryManager _repositories;

        public UserBL(IRepositoryManager repositories)
        {
            _repositories = repositories;
        }

        public async Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext)
        {
            var users = _repositories.Users.GetWhere(x => x.Username == loginModel.Username && x.Password == loginModel.Password);

            if (users == null || !users.Any())
            {
                throw new LogicException("Wrong username or/and password");
            }

            await Authenticate(users.First(), httpContext);
        }

        public Task LogOutAsync(HttpContext httpContext)
        {
            return httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext)
        {
            var users = _repositories.Users.GetWhere(x => x.Username == registerModel.UserName);

            if (users.Any())
            {
                throw new LogicException("Username is already taken");
            }

            var user = new User
            {
                Username = registerModel.UserName,
                Password = registerModel.Password
            };

            _repositories.Users.Add(user);

            await _repositories.SaveChangesAsync();

            await Authenticate(user, httpContext);

            return new UserViewModel
            {
                Id = user.Id,
                Username = user.Username
            };
        }

        private async Task Authenticate(User user, HttpContext httpContext)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Username),
                new Claim("Id",user.Id.ToString())
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}
