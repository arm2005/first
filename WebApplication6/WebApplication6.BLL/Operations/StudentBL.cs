﻿using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication6.Core.Abstractions;
using WebApplication6.Core.Abstractions.Operations;
using WebApplication6.Core.Abstractions.Repositories;
using WebApplication6.Core.Entities;
using WebApplication6.Core.Exceptions;
using WebApplication6.Core.Models;
using WebApplication6.DAL;

namespace WebApplication6.BLL.Operations
{
    public class StudentBL : IStudentBL
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<StudentBL> _logger;

        public StudentBL(IRepositoryManager repositories,ILogger<StudentBL> logger)
        {
            _logger = logger;
            _repositories = repositories;
        }

        public Student AddStudent(CreateStudentModel studentModel)
        {
            var student = new Student
            {
                FirstName = studentModel.FirstName,
                LastName = studentModel.LastName,
                Age = studentModel.Age,
                City = studentModel.City,
                Country = studentModel.Country
            };

            _repositories.Students.Add(student);
            _repositories.SaveChanges();
            return student;
        }

        public void EditStudent(int id, Student student)
        {
            _logger.LogInformation("EditStudent method started");
            var dbStudent = _repositories.Students.Get(id);

            if (dbStudent == null)
            {
                throw new LogicException("Wrong Student Id");
            }

            dbStudent.FirstName = student.FirstName;
            dbStudent.LastName = student.LastName;
            dbStudent.Age = student.Age;
            dbStudent.City = student.City;
            dbStudent.Country = student.Country;

            _repositories.Students.Edit(dbStudent);
            _repositories.SaveChanges();

            _logger.LogInformation("EditStudent method finished");
        }

        public IEnumerable<Student> GetStudents(StudentFilterModel filterModel)
        {


            //if (filterModel.Id.HasValue)
            //{
            //    query = query.Where(x => x.Id == filterModel.Id);
            //}
            List<Student> list;
            if (!string.IsNullOrEmpty(filterModel.FirstName))
            {
                list = _repositories.Students.GetWhere(x => x.FirstName.ToUpper().Contains(filterModel.FirstName.ToUpper())).ToList();
            }
            else
            {
                list = _repositories.Students.GetWhere(x => true).ToList();
            }

            //if (!string.IsNullOrEmpty(filterModel.LastName))
            //{
            //    query = query.Where(x => x.LastName.ToUpper().Contains(filterModel.LastName.ToUpper()));
            //}

            return list;
        }

        public void RemoveStudent(int id)
        {
            var dbStudent = _repositories.Students.Get(id);

            if (dbStudent == null)
            {
                throw new LogicException("Wrong Student Id");
            }

            _repositories.Students.Remove(dbStudent);
            _repositories.SaveChanges();
            
        }

        public async Task TestTransactionAsync()
        {
            using (var transaction = _repositories.BeginTransaction())
            {
                try
                {
                    var student = new Student
                    {
                        FirstName = "Lilit",
                        LastName = "Galstyan",
                        Country = "USA",
                        City = "Los Angeles"
                    };

                    var university = new University
                    {
                        Name = "Yerevan State University"
                    };

                    _repositories.Students.Add(student);
                   // throw new Exception();
                    _repositories.Universities.Add(university);

                    await _repositories.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
