﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication6.Core.Abstractions.Operations;
using WebApplication6.Core.Entities;
using WebApplication6.Core.Models;

namespace WebApplication6.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentBL _studentBL;

        public StudentsController(IStudentBL studentBL)
        {
            _studentBL = studentBL;
        }

        [HttpPost]
        public IActionResult AddStudent([FromBody] CreateStudentModel studentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var addedStudent = _studentBL.AddStudent(studentModel);

            return Created("", addedStudent);
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetStudents([FromQuery] StudentFilterModel filterModel)
        {
            var students = _studentBL.GetStudents(filterModel);

            return Ok(students);
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveStudent([FromRoute] int id)
        {
            _studentBL.RemoveStudent(id);

            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult EditStudent([FromRoute] int id, [FromBody] Student student)
        {
            _studentBL.EditStudent(id, student);

            return Ok();
        }

        [HttpPost("test")]
        public async Task<IActionResult> TestTransactionAsync()
        {
            await _studentBL.TestTransactionAsync();
            return Ok();
        }
    }
}
