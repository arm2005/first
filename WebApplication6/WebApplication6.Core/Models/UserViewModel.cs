﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication6.Core.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Username{ get; set; }
    }
}
