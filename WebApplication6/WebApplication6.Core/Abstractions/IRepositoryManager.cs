﻿using System.Data;
using System.Threading.Tasks;
using WebApplication6.Core.Abstractions.Repositories;

namespace WebApplication6.Core.Abstractions
{
    public interface IRepositoryManager
    {
        IStudentRepository Students { get; }
        IUniversityRepository Universities { get; }
        IUserRepository Users { get; }

        int SaveChanges();
        Task<int> SaveChangesAsync();

        ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted);
    }
}
