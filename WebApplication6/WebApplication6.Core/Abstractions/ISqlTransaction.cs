﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication6.Core.Abstractions
{
    public interface ISqlTransaction :IDisposable
    {
        void Commit();
        void Rollback();
    }
}
