﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication6.Core.Entities;

namespace WebApplication6.Core.Abstractions.Repositories
{
    public interface IUserRepository : ISqlRepository<User>
    {

    }
}
