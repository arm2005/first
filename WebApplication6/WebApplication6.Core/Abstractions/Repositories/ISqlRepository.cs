﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication6.Core.Abstractions.Repositories
{
    public interface ISqlRepository<T>
         where T : class
    {
        T Get(int id);
        void Edit(T entity);
        void Remove(T entity);
        T Add(T entity);
        IEnumerable<T> GetWhere(Func<T,bool> predicate);
    }
}
