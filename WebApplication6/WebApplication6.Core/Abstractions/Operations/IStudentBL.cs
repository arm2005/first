﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication6.Core.Entities;
using WebApplication6.Core.Models;

namespace WebApplication6.Core.Abstractions.Operations
{
    public interface IStudentBL
    {
        IEnumerable<Student> GetStudents(StudentFilterModel filterModel);
        void RemoveStudent(int id);
        void EditStudent(int id, Student student);
        Student AddStudent(CreateStudentModel studentModel);

        Task TestTransactionAsync();
    }
}
