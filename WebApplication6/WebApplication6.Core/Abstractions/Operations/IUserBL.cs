﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication6.Core.Entities;
using WebApplication6.Core.Models;

namespace WebApplication6.Core.Abstractions.Operations
{
   public interface IUserBL
    {
        Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel,HttpContext httpContext);
        Task LoginAsync(UserLoginModel loginModel,HttpContext httpContext);
        Task LogOutAsync(HttpContext httpContext);
    }
}
