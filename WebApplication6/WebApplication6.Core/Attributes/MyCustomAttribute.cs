﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication6.Core.Entities;

namespace WebApplication6.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MyCustomAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is Student s)
            {
                return s.City != "Moscow";
            }

            return true;
        }
    }
}
