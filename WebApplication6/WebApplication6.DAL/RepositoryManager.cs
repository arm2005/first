﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebApplication6.Core.Abstractions;
using WebApplication6.Core.Abstractions.Repositories;
using WebApplication6.DAL.Repositories;

namespace WebApplication6.DAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly BasicDbContext _dbContext;

        public RepositoryManager(BasicDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private IStudentRepository _students;
        public IStudentRepository Students => _students ?? (_students = new StudentRepository(_dbContext));

        private IUniversityRepository _universities;
        public IUniversityRepository Universities => _universities ?? (_universities = new UniversityRepository(_dbContext));

        private IUserRepository _users;
        public IUserRepository Users => _users ?? (_users = new UserRepository(_dbContext));

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(_dbContext, isolation);
        }
    }
}
