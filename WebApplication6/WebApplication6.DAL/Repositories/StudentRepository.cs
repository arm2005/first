﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication6.Core.Abstractions.Repositories;
using WebApplication6.Core.Entities;

namespace WebApplication6.DAL.Repositories
{
    public class StudentRepository : SqlRepositoryBase<Student>, IStudentRepository
    {
        public StudentRepository(BasicDbContext dbContext)
            : base(dbContext)
        {

        }
    }
}
