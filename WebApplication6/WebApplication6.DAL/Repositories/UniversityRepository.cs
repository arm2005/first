﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication6.Core.Abstractions.Repositories;
using WebApplication6.Core.Entities;

namespace WebApplication6.DAL.Repositories
{
   public class UniversityRepository :SqlRepositoryBase<University>, IUniversityRepository
    {
        public UniversityRepository(BasicDbContext dbContext)
           : base(dbContext)
        {

        }
    }
}
