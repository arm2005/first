﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication6.Core.Abstractions.Repositories;

namespace WebApplication6.DAL.Repositories
{
   public class SqlRepositoryBase<T> : ISqlRepository<T>
       where T : class
    {
        private readonly BasicDbContext _dbContext;

        public SqlRepositoryBase(BasicDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public T Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            return entity;
        }

        public void Edit(T entity)
        {
            _dbContext.Set<T>().Update(entity);
        }

        public T Get(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public IEnumerable<T> GetWhere(Func<T,bool> predicate)
        {
           return _dbContext.Set<T>().Where(predicate);
        }

        public void Remove(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
        }

        public int SaveChanges()
        {
           return  _dbContext.SaveChanges();
        }
    }
}
