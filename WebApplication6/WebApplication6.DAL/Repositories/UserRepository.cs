﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication6.Core.Abstractions.Repositories;
using WebApplication6.Core.Entities;

namespace WebApplication6.DAL.Repositories
{
    public class UserRepository : SqlRepositoryBase<User>, IUserRepository
    {
        public UserRepository(BasicDbContext dbContext) 
            : base(dbContext)
        {
        }
    }
}
