﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication6.Core.Entities;

namespace WebApplication6.DAL
{
    public class BasicDbContext : DbContext
    {
        public BasicDbContext(DbContextOptions options)
            : base(options)
        {

        }
        public DbSet<Student> Students { get; set; }
        public DbSet<University> Universities { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>(x =>
            {
                x.ToTable("Students");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.FirstName).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.LastName).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.Country).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.City).HasColumnType("NVARCHAR(50)");
                x.Property(x => x.Age).HasColumnType("INTEGER");
            });

            modelBuilder.Entity<University>(x => 
            {
                x.ToTable("Universities");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.Name).HasColumnType("NVARCHAR(100)");
            });

            modelBuilder.Entity<User>(x => 
            {
                x.ToTable("Users");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();
                x.Property(x => x.Username).HasColumnType("NVARCHAR(100)");
                x.HasIndex(x=>x.Username).IsUnique();
                x.Property(x => x.Password).HasColumnType("NVARCHAR(100)");
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
